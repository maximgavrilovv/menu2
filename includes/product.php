<?php

include "config.php";

class Product {

    protected $description;
    protected $name;
    protected $price;
    protected $id;

    public $tableName = 'products';

    public $colName = 'name';
    public $colDescription = 'description';
    public $colPrice = 'price';

    public function save() {
        $conn = new DbConfig();
        $connection = $conn->connect();

        $name = $_POST['name1'];
        $price = $_POST['price'];
        $description =  $_POST['test'];

        $filename = $_FILES['image']['name'];
        $filetmpname = $_FILES['image']['tmp_name'];
        $folder = 'images/';

        if (file_exists($folder.$filename)) {
            // echo "already exists. please rename";
            // return;
            $filename = $filename . "q";
        }
        move_uploaded_file($filetmpname, $folder.$filename);

        if ($price == "" && $name == "" && $description == "") {
            return; //avoiding form re-submition with empty values
        }
    
        $sql = "INSERT INTO products VALUES (NULL, '$name', '$description', '$price', '$filename')";
        echo $filename;
        if (mysqli_query($connection, $sql)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($connection);
        } 
    }

    public function update() {

        $conn = new DbConfig();
        $connection = $conn->connect();
        
        $id = $_POST['id'];
        echo $id;
        $name = $_POST['name2'];
        $price = $_POST['price2'];
        $description =  $_POST['test2'];

        if ($price == "" && $name == "" && $description == "") {
            return; //avoiding form re-submition with empty values
        }

        $filename = $_FILES['image2']['name'];
        $filetmpname = $_FILES['image2']['tmp_name'];
        $folder = 'images/';

        move_uploaded_file($filetmpname, $folder.$filename);
        
        $sql = "UPDATE products SET name='$name', description='$description', price='$price', image='$filename' WHERE id='$id'";
        
        if (mysqli_query($connection, $sql)) {
            echo "Record edited successfully";
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($connection);
        }
    }

    public function delete($id) {
        $conn = new DbConfig();
        $connection = $conn->connect();

        $sql = "DELETE from products WHERE id='$id'";
        if (mysqli_query($connection, $sql)) {
            echo "Record deleted successfully";
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($connection);
        }
    }

    public function getRecord($id) {
        $conn = new DbConfig();
        $connection = $conn->connect();
        $query = "SELECT * FROM products WHERE id='$id'";
        return $query;
    }

    public function getDescription() {
        return $this->sku;
    }

    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setDescription($sku) {
        $this->sku = $sku;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public static function show($rproduct) {
        $conn = new DbConfig();
        echo '<tr>
                <td> <img src="images/' . $rproduct['image'] . '" width="100" height="100"</td>
                <td>' . $rproduct['name'] . '</td>
                <td>' . $rproduct['description'] . '</td>
                <td>' . $rproduct['price'] . '</td>
                <td><a href="edit.php?U_ID='. $rproduct['id'] . '" class="btn btn-success">Edit</td>
                <td><a href="delete.php?D_ID='. $rproduct['id'] . '" class="btn btn-danger" name="del">Delete</td>
            </tr>';
    }
    
}

?>