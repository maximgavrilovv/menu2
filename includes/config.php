<?php

class DbConfig {

    private $server = '127.0.0.1';
    private $username = 'root';
    private $password = '';
    private $db_name = 'menu_db';

    private $tableName = 'products';

    private $query;

    public function connect() {
        $connection = mysqli_connect($this->server, $this->username, $this->password, $this->db_name);
        if ($connection == false) {
            echo 'failed to connect to db!';
            return;
        } else {
            //echo 'connected ';
        }
        return $connection;
    }
    
    //Getters and setters
    public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    public function getServer() {
        return $this->server;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getDbName() {
        return $this->db_name;
    }

    public function setServer($serv) {
        $this->server = $serv;
    }

    public function setUsername($user) {
        $this->username = $user;
    }   

    public function setPassword($pass) {
        $this->password = $pass;
    }

    public function setDbName($dbname) {
        $this->$db_name = $dbname;
    }
}

?>