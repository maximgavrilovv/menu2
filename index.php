<?php
require "includes/product.php";

class DbAdd {

  private $conn;
  private $connection;
  private $name;
  private $price;
  private $description;

  public function start() { 
    $product = new Product();
    if (isset($_POST['uploadfilesub'])) {
      $product->save();
    }
   if (isset($_POST['edit'])) {
      $product->update();
   }
   if (isset($_POST['del'])) {
     echo 123;
   }
  }
}
$d = new DbAdd();
$d->start();

$conn = new DbConfig();
$connection = $conn->connect();

$rproduct = mysqli_query($connection, "SELECT * FROM `products`");
$res = mysqli_query($connection, "SELECT image FROM products");

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="/script.js"></script>
<title>Menu</title>
</head>
<body>
<nav class="navbar navbar-default" style="background-color: rgb(214, 209, 179);">
  <div class="container-fluid">
    <div class="navbar-header navbar-brand">
      Test
    </div>
  </div>
</nav>
<h2 id="t">
  Product List 
</h2>
<hr style="border-top: 1px solid black; margin-top: 1%;"/>
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-auto">
  <table class="table table-responsive table-hover" id="t">
    <thead>
      <tr class="table-active">
        <td>Image</td>
        <td>Name</td>
        <td>Description</td>
        <td>Price</td>
        <td>Edit</td>
        <td>Delete</td>
      </tr>
    </thead>
    <tbody>
    <?php
            while ( ($sproduct = mysqli_fetch_assoc($rproduct)) ) {  
              Product::show($sproduct);
            } 
    ?> 
    </tbody>
  </table>
  </div>
  </div>
</div>
<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
<div class="row justify-content-center">
<div class="col-auto">
<button class="btn btn-success btn-lg " style="margin-left: 1%;"  onclick="goToAdd()">Add</button>
</div>
</div>
</body>
</html>
<style>
#t {
  font-color: red;
}
</style>