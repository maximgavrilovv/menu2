<?php
include "includes/config.php";

$conn = new DbConfig();
$connection = $conn->connect();

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<title>Update</title>
<script> src="script.js"</script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color: rgb(214, 209, 179);">
  <div class="container-fluid">
    <div class="navbar-header navbar-brand">
      Test 
    </div>
  </div>
</nav>
<div id="get"></div>
<h2>
  Product Add 
  <button class="btn btn-success btn-lg float-right" style="margin-right: 1%;" onclick="goToList()">Product List</button>
</h2>
<hr style="border-top: 1px solid black; margin-top: 1.5%;"/>
<div class="container-fluid">
  <form role="form" action="/index.php" onsubmit="check()" method="post" enctype="multipart/form-data">
    <div class="form-group col-lg-3">
      <label for="NAME">Name</label>
      <input type="text" class="form-control form-control-lg" id="name1" name="name1">
    </div>
    <div class="form-group col-lg-3">
      <label for="PRICE">Price(euro)</label>
      <input type="text" class="form-control form-control-lg" id="price" name="price">
    </div>
    <div class="form-group col-lg-3">
      <label for="test">Description</label>
      <input type="text" class="form-control form-control-lg" id="test" name="test">
    </div>
    <div class="form-group col-lg-3">
      <input type="file" name="image">
    </div>
    <button type="submit" class="btn btn-primary" style="margin-top: 2%;" name="uploadfilesub">Save</button>
  </form>
</div>
</div>
</body>
</html>